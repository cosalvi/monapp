import React, { useState } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import axios from 'axios';

export default function Carte(props) {

  const [data, setData] = useState( 
    async () => {
      const result = await axios(
      'https://api.openweathermap.org/data/2.5/weather?q='+props.ville+'&appid=3a6b6bb65df5947a803434ce37f3f938',
      );
      setData(result.data);
      setIsLoading(false);
      console.log("test loading data");
      console.log(result.data);
      console.log(isLoading);
  });


  const [isLoading, setIsLoading] = useState(true);

  return (
    <View style={styles.container}>
    <Text style={styles.paragraph}>
      {data.name}
    </Text>
      <Text style={styles.paragraph}>
        {data.timezone}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 24,
  },
  paragraph: {
    margin: 24,
    marginTop: 0,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});
